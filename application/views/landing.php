<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <title>eLensaku</title>
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="" name="keywords">
    <meta content="" name="description">

    <!-- Favicons -->
    <link rel="shortcut icon" href="<?= base_url('public/') ?>img/icons.png" type="image/png">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,700,700i|Roboto:100,300,400,500,700|Philosopher:400,400i,700,700i" rel="stylesheet">

    <!-- Bootstrap css -->
    <!-- <link rel="stylesheet" href="css/bootstrap.css"> -->
    <link href="<?= base_url('public/') ?>lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Libraries CSS Files -->
    <link href="<?= base_url('public/') ?>lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
    <link href="<?= base_url('public/') ?>lib/owlcarousel/assets/owl.theme.default.min.css" rel="stylesheet">
    <link href="<?= base_url('public/') ?>lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?= base_url('public/') ?>lib/animate/animate.min.css" rel="stylesheet">
    <link href="<?= base_url('public/') ?>lib/toastr/toastr.min.css" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
    <link href="<?= base_url('public/') ?>lib/modal-video/css/modal-video.min.css" rel="stylesheet">
    <link href="<?= base_url('public/') ?>lib/swal/sweetalert2.css" rel="stylesheet">

    <!-- Main Stylesheet File -->
    <link href="<?= base_url('public/') ?>css/style.css" rel="stylesheet">

    <!-- <script src="<?//= base_url('public/') ?>lib/jquery/jquery.min.js"></script> -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/jquery/jquery-migrate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
</head>

<body>
    <script type="text/javascript">
        <?php if ($this->session->flashdata('berhasil')) { ?>
            toastr.success("<?php echo $this->session->flashdata('berhasil'); ?>");
        <?php } else if ($this->session->flashdata('gagal')) {  ?>
            toastr.error("<?php echo $this->session->flashdata('gagal'); ?>");
        <?php } else if ($this->session->flashdata('sukses')) {  ?>
            toastr.success("<?php echo $this->session->flashdata('sukses'); ?>");
        <?php } else if ($this->session->flashdata('info')) {  ?>
            toastr.info("<?php echo $this->session->flashdata('info'); ?>");
        <?php } ?>
    </script>
    <header id="header" class="header header-hide">
        <div class="container">
            <div id="logo" class="pull-left">
                <h1><img width="170px" src="<?= base_url('public/') ?>img/iconlensaku.png" alt=""></h1>
            </div>

            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li class="menu-active"><a href="#hero">Home</a></li>
                    <li><a href="#get-started">Tentang Kami</a></li>
                    <li><a href="#features">Pilhan Jasa</a></li>
                    <li><a href="#screenshots">Portfolio</a></li>
                    <li><a href="#team">Profesional Team</a></li>
                    <li><a href="#testimonials">Testimonial</a></li>
                    <li><a href="#pricing">Harga</a></li>
                    <li><a href="#contact">Kontak kami</a></li>

                </ul>
            </nav>
        </div>
    </header>

    <section id="hero" class="wow fadeIn">
        <div class="hero-container">
            <br>
            <h1>Welcome to eLensaku</h1>
            <h2>Situs jasa fotografi dengan cepat dan efektif</h2>
            <img src="<?= base_url('public/') ?>img/dashb.png" width="680" height="428px" alt="Hero Imgs">
            <a href="#get-started" class="btn-get-started scrollto"><i class="fa fa-play"></i> Mulai</a>
            <!-- <div class="btns">
                <a href="#"><i class="fa fa-apple fa-3x"></i> App Store</a>
                <a href="#"><i class="fa fa-play fa-3x"></i> Google Play</a>
                <a href="#"><i class="fa fa-windows fa-3x"></i> windows</a>
            </div> -->
        </div>
    </section>

    <section id="get-started" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2><img width="170px" src="<?= base_url('public/') ?>img/iconlensaku.png" alt=""></h2>
                <p class="separator">Cari fotografer terpercaya untuk kebutuhan anda .</p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-4">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/trusted.svg" alt="img" class="img-fluid">
                        <h4>Terpercaya</h4>
                        <p>Kami jasa fotografer terpercaya dan bertanggung jawab yang menyelesaikan pekerjaan sampai selesai</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/uang.svg" alt="img" class="img-fluid">
                        <h4>Harga yang terjangkau</h4>
                        <p>Kami jasa fotografer yang memiliki harga terjangkau , sehingga ramah di kantong</p>

                    </div>
                </div>

                <div class="col-md-6 col-lg-4">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/foto.svg" alt="img" class="img-fluid">
                        <h4>Hasil berkualitas</h4>
                        <p>Kami fotografer yang memiliki hasil foto yang berkualitas meskipun memiliki harga yang terjangkau</p>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="features" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2>Pilihan Jasa Fotografi.</h2>
                <p class="separator">List pilihan jasa fotografi yang bisa anda pilih .</p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/prewedding.svg" alt="img" class="img-fluid">
                        <h4>Prewedding Moment</h4>
                        <p>Kami menerima pilihan jasa fotografi untuk prewedding anda untuk mengabadikan momen sebelum pernikahan</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/wedding.svg" alt="img" class="img-fluid">
                        <h4>Wedding Moment</h4>
                        <p>Kami menerima pilihan jasa fotografi untuk wedding anda untuk mengabadikan momen pernikahan yang anda inginkan</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/keluarga.svg" alt="img" class="img-fluid">
                        <h4>Foto Keluarga</h4>
                        <p>Kami menerima pilihan jasa fotografi untuk moment anda bersama keluarga anda tercinta untuk di abadikan</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/event.svg" alt="img" class="img-fluid">
                        <h4>Event Kegiatan</h4>
                        <p>Kami menerima pilihan jasa fotografi event kegiatan anda agar bisa di buat kenang - kenangan di kemudian hari. Seperti Gathering, seminar, dan lain-lain</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/produk.svg" alt="img" class="img-fluid">
                        <h4>Foto Produk</h4>
                        <p>Kami menerima pilihan jasa fotografi yaitu foto produk agar berfungsi untuk mempromosikan produk anda</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/wisuda.svg" alt="img" class="img-fluid">
                        <h4>Foto Wisuda</h4>
                        <p>Kami menerima pilihan jasa fotografi yaitu foto wisuda agar bisa di abadikan dan dijadikan kenang - kenangan</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/baby.svg" alt="img" class="img-fluid">
                        <h4>Baby Shoot</h4>
                        <p>Kami menerima pilihan jasa fotografi lainnya yaitu baby shoot agar bisa dijadikan kenang - kenangan</p>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="feature-block">
                        <img src="<?= base_url('public/') ?>img/svg/photobooth.svg" alt="img" class="img-fluid">
                        <h4>Photobooth</h4>
                        <p>Kami menerima pilihan jasa fotografi lainnya yaitu Photobooth untuk memeriahkan acara anda dan tamu anda sebagai kenang kenangan di suatu hari nanti</p>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="screenshots" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2>Portfolio</h2>
                <p class="separator">Hasil portfolio foto yang berkualitas .</p>
            </div>
        </div>

        <div class="container">
            <div class="owl-carousel owl-theme">
                <!-- <div><img src="img/screen/c1.jpg" alt="img"></div>
                <div><img src="img/screen/c2.jpg" alt="img"></div> -->
                <div><img src="<?= base_url('public/') ?>img/screen/c3.jpg" alt="img"></div>
                <div><img src="<?= base_url('public/') ?>img/screen/c4.jpg" alt="img"></div>
                <div><img src="<?= base_url('public/') ?>img/screen/c5.jpg" alt="img"></div>
                <div><img src="<?= base_url('public/') ?>img/screen/c6.jpg" alt="img"></div>
                <div><img src="<?= base_url('public/') ?>img/screen/c7.jpg" alt="img"></div>
                <div><img src="<?= base_url('public/') ?>img/screen/c8.jpg" alt="img"></div>
                <!-- <div><img src="img/screen/1.jpg" alt="img"></div>
                <div><img src="img/screen/2.jpg" alt="img"></div>
                <div><img src="img/screen/3.jpg" alt="img"></div>
                <div><img src="img/screen/4.jpg" alt="img"></div>
                <div><img src="img/screen/5.jpg" alt="img"></div>
                <div><img src="img/screen/6.jpg" alt="img"></div>
                <div><img src="img/screen/7.jpg" alt="img"></div>
                <div><img src="img/screen/8.jpg" alt="img"></div>
                <div><img src="img/screen/9.jpg" alt="img"></div> -->
            </div>
        </div>
    </section>

    <section id="video" class="text-center wow fadeInUp">
        <div class="overlay">
            <div class="container-fluid container-full">

                <div class="row">
                    <a href="#" class="js-modal-btn play-btn" data-video-id="s22ViV7tBKE"></a>
                </div>

            </div>
        </div>
    </section>


    <section id="team" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2>Tim eLensaku</h2>
                <p class="separator">Professional Tim eLensaku .</p>
            </div>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-md-6 col-md-4 col-lg-3">
                    <div class="team-block bottom">
                        <img src="<?= base_url('public/') ?>img/team/fajar.jpg" class="img-responsive" alt="img">
                        <div class="team-content">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <span>Founder eLensaku</span>
                            <h4>Anis Fajar Prakoso</h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-4 col-lg-3">
                    <div class="team-block bottom">
                        <img src="<?= base_url('public/') ?>img/team/iqbal.jpg" class="img-responsive" alt="img">
                        <div class="team-content">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <span>Photographer</span>
                            <h4>Maulana Iqbal</h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-4 col-lg-3">
                    <div class="team-block bottom">
                        <img src="<?= base_url('public/') ?>img/team/rike.jpg" class="img-responsive" alt="img">
                        <div class="team-content">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <span>Photographer</span>
                            <h4>Rike Maylina</h4>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-md-4 col-lg-3">
                    <div class="team-block bottom">
                        <img src="<?= base_url('public/') ?>img/team/farid.jpg" class="img-responsive" alt="img">
                        <div class="team-content">
                            <ul class="list-unstyled">
                                <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
                            </ul>
                            <span>Photographer</span>
                            <h4>Farid Anharul Ilmi</h4>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="testimonials" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <div class="testimonials-content">
                        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                            <div class="carousel-inner" role="listbox">
                                <div class="carousel-item  active">
                                    <div class="top-top">
                                        <h2>Testimoni</h2>
                                        <img src="<?= base_url('public/') ?>img/testi/1.jpg" width="200px" height="200px" class="rounded-circle z-depth-2" alt="100x100" alt="img">
                                        <br><br>
                                        <p>Hasil berkualitas dan harga terjangkau sehingga ramah di kantong</p>
                                        <h4>Ahmad Budi<span>Karyawan Swasta</span></h4>
                                    </div>
                                </div>

                                <div class="carousel-item ">
                                    <div class="top-top">
                                        <h2>Testimoni</h2>
                                        <img src="<?= base_url('public/') ?>img/testi/2.jpg" width="190px" height="200px" class="rounded-circle z-depth-2" alt="100x100" alt="img">
                                        <br><br>
                                        <p>Hasil foto yang memuaskan dan juga memiliki harga yang relatif tidak mahal sehingga masih bisa di jangkau. Recomended sekali</p>
                                        <h4>Feri R.<span>Wiraswasta</span></h4>
                                    </div>
                                </div>

                                <div class="carousel-item ">
                                    <div class="top-top">
                                        <h2>Testimoni</h2>
                                        <img src="<?= base_url('public/') ?>img/testi/4r.jpg" width="190px" height="200px" class="rounded-circle z-depth-2" alt="100x100" alt="img">
                                        <br><br>
                                        <p>Hasil keren dan memuaskan dan fotografer juga ramah sehingga bisa di ajak diskusi untuk pemilihan konsep foto</p>
                                        <h4>Nisa Nida<span>Karyawan Swasta</span></h4>
                                    </div>
                                </div>
                            </div>
                            <div class="btm-btm">
                                <ul class="list-unstyled carousel-indicators">
                                    <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="1"></li>
                                    <li data-target="#carousel-example-generic" data-slide-to="2"></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </section>

    <section id="pricing" class="padd-section text-center wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2>Harga pilihan jasa</h2>
                <p class="separator">Harga jasa untuk pemotretan foto .</p>
            </div>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-lg-3">
                    <div class="block-pricing">
                        <div class="table">
                            <h4>basic</h4>
                            <h2>Rp. 350.000</h2>
                            <ul class="list-unstyled">
                                <li>1.5 Jam</li>
                                <li>15x Fotoshoot</li>
                                <li>Cetak foto 3 lembar</li>
                            </ul>
                            <div class="table_btn">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="block-pricing">
                        <div class="table">
                            <h4>PERSONAL</h4>
                            <h2>Rp. 500.000</h2>
                            <ul class="list-unstyled">
                                <li>2 Jam</li>
                                <li>25x Fotoshoot</li>
                                <li>Cetak foto 5 lembar</li>
                            </ul>
                            <div class="table_btn">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="block-pricing">
                        <div class="table">
                            <h4>BUSINESS</h4>
                            <h2>Nego</h2>
                            <ul class="list-unstyled">
                                <li>Di bicarakan secara langsung</li>
                            </ul>
                            <div class="table_btn">
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-6 col-lg-3">
                    <div class="block-pricing">
                        <div class="table">
                            <h4>profeesional</h4>
                            <h2>Nego</h2>
                            <ul class="list-unstyled">
                                <li>Di bicarakan secara langsung</li>
                                <div class="table_btn">
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--==========================
    Contact Section
  ============================-->
    <section id="contact" class="padd-section wow fadeInUp">
        <div class="container">
            <div class="section-title text-center">
                <h2>Kontak kami</h2>
                <p class="separator">Silahkan tinggalkan pesan untuk menghubungi kami dengan lanjut atau kunjungi kami di alamat berikut</p>
            </div>
        </div>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-3 col-md-4">
                    <div class="info">
                        <div>
                            <i class="fa fa-map-marker"></i>
                            <p>Ds. Bakalan Krapyak RT 05/01<br>Kudus, Jawa Tengah, Indonesia 59332</p>
                        </div>
                        <div class="email">
                            <i class="fa fa-envelope"></i>
                            <p>anisfajar.kudus@gmail.com</p>
                        </div>
                        <div>
                            <i class="fa fa-phone"></i>
                            <p>+62 8572-7109-286</p>
                        </div>
                    </div>
                    <div class="social-links">
                        <a href="#" class="facebook"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="instagram"><i class="fa fa-instagram"></i></a>
                        <a href="#" class="linkedin"><i class="fa fa-linkedin"></i></a>
                    </div>
                </div>

                <div class="col-lg-5 col-md-8">
                    <div class="form">
                        <div id="sendmessage">Silahkan tinggalkan pesan dengan sopan</div>
                        <div id="errormessage"></div>
                        <form action="<?= site_url('pesan') ?>" method="post" role="form" class="contactForm">
                            <!-- <input type="hidden" value="1338369163" name="telegramid"> -->
                            <div class="form-group">
                                <label>Nama : </label>
                                <input type="text" onkeypress="return huruf(event)" name="namapengirim" autocomplete="off" class="form-control" id="name" placeholder="Masukkan Nama" data-rule="minlen:4" data-msg="Please enter at least 4 chars" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>Email : </label>
                                <input type="email" autocomplete="off" class="form-control" name="email" id="email" placeholder="Masukkan Email" data-rule="email" data-msg="Please enter a valid email" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>Subject pesan : </label>
                                <input type="text" autocomplete="off" class="form-control" name="subject" id="subject" placeholder="Masukkan Subject Pesan" data-rule="minlen:4" data-msg="Please enter at least 8 chars of subject" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>Pesan : </label>
                                <textarea class="form-control" name="message" rows="5" data-rule="required" data-msg="Please write something for us" placeholder="Isi Pesan"></textarea>
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>Sosial Media : </label>
                                <input type="text" autocomplete="off" class="form-control" name="sosmed" id="sosmed" placeholder="contoh : IG (@fajar) / Twitter (@aku)" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>No. HP : </label>
                                <input type="text" onkeypress="return hanyaAngka(event)" maxlength="13" autocomplete="off" class="form-control" name="nohp" id="nohp" placeholder="Masukkan No. Hp" />
                                <div class="validation"></div>
                            </div>
                            <div class="form-group">
                                <label>Pilihan Jasa</label>
                                <select class="form-control" name="pilihan" id="pilihjasa">
                                    <option value="basic">BASIC</option>
                                    <option value="personal">PERSONAL</option>
                                    <option value="business">BUSINESS</option>
                                    <option value="profesional">PROFEESIONAL</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <button type="submit" class="btn btn-info"><i class="fa fa-send"></i> Kirim Pesan</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <footer class="footer">
        <div class="container">
            <div class="row">
                <div class="col-md-12 col-lg-4">
                    <div class="footer-logo">

                        <a class="navbar-brand" href="#">eLensaku</a>
                        <p>eLensaku merupakan sebuah jasa fotografi yang terpercaya dan hasil yang berkualitas dan juga memiliki harga yang terjangkau. </p>
                    </div>
                </div>

            </div>
        </div>

        <div class="copyrights">
            <div class="container">
                <p>&copy; Copyrights eLensaku 2020. All rights reserved.</p>
                <div class="credits">
                </div>
            </div>
        </div>
    </footer>

    <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>

    <!-- JavaScript Libraries -->

    <script src="<?= base_url('public/') ?>lib/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/superfish/hoverIntent.js"></script>
    <script src="<?= base_url('public/') ?>lib/superfish/superfish.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/easing/easing.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/modal-video/js/modal-video.js"></script>
    <script src="<?= base_url('public/') ?>lib/owlcarousel/owl.carousel.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/wow/wow.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/toastr/toastr.min.js"></script>
    <script src="<?= base_url('public/') ?>lib/swal/sweetalert2.min.js"></script>
    <!-- Contact Form JavaScript File -->
    <!-- <script src="<?//= base_url('public/') ?>contactform/contactform.js"></script> -->

    <!-- Template Main Javascript File -->
    <script src="<?= base_url('public/') ?>js/main.js"></script>
    <script>
        function hanyaAngka(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if (charCode > 31 && (charCode < 48 || charCode > 57))

                return false;
            return true;
        }

        function huruf(evt) {
            var charCode = (evt.which) ? evt.which : event.keyCode
            if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122) && charCode > 32) {
                return false;
            }
            return true;
        }
    </script>

</body>

</html>
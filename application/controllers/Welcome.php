<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		date_default_timezone_set("Asia/Bangkok");
		$this->load->model('m_welcome');
	}

	public function index()
	{
		$this->load->view('landing');
	}

	public function pesan()
	{
		$tokenbot = '1338369163:AAHDrSpV6_9_w-TDMYiZAahpmEhr-FLFvUI';
		$chatidbot = '286235907';
		$namapengirim = $this->input->post('namapengirim');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');
		$sosmed = $this->input->post('sosmed');
		$nohp = $this->input->post('nohp');
		$pilihan = $this->input->post('pilihan');

		$API = "https://api.telegram.org/bot" . $tokenbot . "/sendmessage?chat_id=" . $chatidbot . "&text=Halo, ada yang mengirim pesan di elensaku silahkan cek di website dengan atas nama $namapengirim ($nohp) dengan pemilihan jasa yaitu " . strtoupper($pilihan);

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
		curl_setopt($ch, CURLOPT_CAINFO, 'C:\cacert.pem');
		curl_setopt($ch, CURLOPT_URL, $API);
		curl_exec($ch);
		curl_close($ch);

		$data = [
			'nama_pengirim' => $namapengirim,
			'email_pengirim' => $email,
			'subject_pesan' => $subject,
			'isi_pesan' => $message,
			'sosial_media' => $sosmed,
			'nohp_pengirim' => $nohp,
			'pilihan' => $pilihan,
			'created_at' => date("Y-m-d H:i:s"),
			'updated_at' => date("Y-m-d H:i:s")
		];

		$this->m_welcome->savepesan($data);
		$this->session->set_flashdata('berhasil', 'Kirim pesan berhasil');

		redirect(site_url('/'));
	}

	public function request_url()
	{
		$token 		= 'bot1338369163:AAHDrSpV6_9_w-TDMYiZAahpmEhr-FLFvUI';
		$updates 	= file_get_contents("php://input");

		$updates 	= json_decode($updates, true);
		$pesan 		= $updates['message']['text'];
		$chat_id 	= '286235907';
		$pesan 		= strtolower($pesan);
		$data 		= $this->m_welcome->getdata()->result();

		/* if ($pesan == 'list booking') {
			if (count($data) > 0) {
				foreach ($data as $value) {
					echo "<br>";
					$row = $value->nama_pengirim;
					$pesan_balik = $row;
					$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_exec($ch);
					curl_close($ch);
				}
			} else {
				$pesan_balik = 'Maaf, data tidak ada';
				$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_exec($ch);
				curl_close($ch);
			}
		} else {
			$pesan_balik = 'Maaf, kata kunci tidak sesuai';
			$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_exec($ch);
			curl_close($ch);
		} */

		if ($pesan == 'list booking') {
			if (count($data) > 0) {
				foreach ($data as $value) {
					echo "<br>";
					$row = $value->nama_pengirim;
					$pesan_balik = $row;
					$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
					$ch = curl_init();
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
					curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_exec($ch);
					curl_close($ch);
				}
			} else {
				$pesan_balik = 'Maaf, data tidak ada';
				$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
				curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
				curl_setopt($ch, CURLOPT_URL, $url);
				curl_exec($ch);
				curl_close($ch);
			}
		} elseif ($pesan == 'booking selesai') {
			$row = $this->m_welcome->getcountselesai()->row();
			$pesan_balik = "Jumlah booking yang sudah selesai di proses sebanyak : " . $row->jml . " data";
			$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_exec($ch);
			curl_close($ch);
		} elseif ($pesan == 'booking menunggu') {
			$row = $this->m_welcome->getcountmenunggu()->row();
			$pesan_balik = "Jumlah booking yang masih menunggu untuk di proses sebanyak : " . $row->jml . " data";
			$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_exec($ch);
			curl_close($ch);
		} else {
			$pesan_balik = 'Maaf, kata kunci tidak sesuai';
			$url = "https://api.telegram.org/$token/sendMessage?parse_mode=markdown&chat_id=$chat_id&text=$pesan_balik";
			$ch = curl_init();
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
			curl_setopt($ch, CURLOPT_URL, $url);
			curl_exec($ch);
			curl_close($ch);
		}
	}
}

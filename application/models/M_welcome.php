<?php

class M_welcome extends CI_Model
{
    public function savepesan($data)
    {
        $this->db->insert('inbox', $data);
        return true;
    }

    public function getdata()
    {
        $this->db->select('*');
        $this->db->from('inbox');
        $this->db->where('is_finish', '0');
        return $this->db->get();
    }

    public function getcountselesai()
    {
        $this->db->select("COUNT(*) as jml");
        $this->db->from('inbox');
        $this->db->where('is_finish', '1');
        return $this->db->get();
    }

    public function getcountmenunggu()
    {
        $this->db->select("COUNT(*) as jml");
        $this->db->from('inbox');
        $this->db->where('is_finish', '0');
        return $this->db->get();
    }
}
